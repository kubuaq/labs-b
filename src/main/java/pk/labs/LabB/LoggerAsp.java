package pk.labs.LabB;

import org.aspectj.lang.JoinPoint;
import org.springframework.stereotype.Component;

import pk.labs.LabB.Contracts.Logger;
@Component("log")
public class LoggerAsp{
	private Logger logger;
	void setLogger(Logger logger)
	{
		this.logger =logger;
	}
	public void logMethodExit(JoinPoint joinPoint, Object result) {
		if(logger!=null)
			logger.logMethodExit(joinPoint.getSignature().getName(), result);
	}
	public void logMethodEntrance(JoinPoint joinPoint) {
		if(logger!=null)
			logger.logMethodEntrance(joinPoint.getSignature().getName(), joinPoint.getArgs());
	}
}
