package pk.labs.LabB;

import pk.labs.LabB.display.DisplayBean;
import pk.labs.LabB.controlpanel.ControlPanelBean;
import pk.labs.LabB.Contracts.Express;
import pk.labs.LabB.main.ExpressBean;
public class LabDescriptor {
    // region P1
    public static String displayImplClassName = DisplayBean.class.getName();
    public static String controlPanelImplClassName = ControlPanelBean.class.getName();

    public static String mainComponentSpecClassName = Express.class.getName();
    public static String mainComponentImplClassName = ExpressBean.class.getName();
    public static String mainComponentBeanName = "express";
    // endregion

    // region P2
    public static String mainComponentMethodName = "ZaparzKawe";
    public static Object[] mainComponentMethodExampleParams = new Object[] {"kawa"};
    // endregion

    // region P3
    public static String loggerAspectBeanName = "log";
    // endregion
}
