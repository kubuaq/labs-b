package pk.labs.LabB.ui;

import javax.swing.JPanel;
import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Display;

@Component
public class InvertedDisplay implements Negativeable {

    Utils utils;

    public InvertedDisplay() {
        this.utils = new Utils();
    }

    @Override
    public void negative() {
        this.utils.negateComponent(((Display) AopContext.currentProxy()).getPanel());

    }
}
