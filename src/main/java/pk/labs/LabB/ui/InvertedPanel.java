package pk.labs.LabB.ui;

import javax.swing.JPanel;
import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.ControlPanel;

@Component
public class InvertedPanel implements Negativeable {
    Utils utils;
    
    public InvertedPanel() {
        this.utils = new Utils();
    }

    @Override
    public void negative() {
        this.utils.negateComponent(((ControlPanel)AopContext.currentProxy()).getPanel());
    }
}
